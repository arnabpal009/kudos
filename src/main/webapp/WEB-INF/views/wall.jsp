
<!DOCTYPE html>
<html>

    <head>
        <title>Feeds || Kudos</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../resources/css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="../resources/css/font-awesome.min.css">
        <link rel="stylesheet" href="../resources/css/bootstrap-switch.min.css">
        <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,600,700" rel="stylesheet">
        <link rel="stylesheet" href="../resources/css/style.css" type="text/css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" />
    </head>

    <body id="wall">

        <!--Header with Nav -->
        <header class="text-right">
            <div class="text-left search">
                <input name="q" type="text" placeholder="Search..">
            </div>
            <div class="menu-icon">
                <div class="dropdown">
                    <span class="dropdown-toggle" role="button" id="dropdownSettings" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        <span class="hidden-xs hidden-sm">Settings</span> <i class="fa fa-cogs" aria-hidden="true"></i>
                    </span>
                    <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownSettings">
                        <li>
                            <a href="#" title="Settings" data-toggle="modal">
                                <div class="col-xs-4">
                                    <i class="fa fa-wrench" aria-hidden="true"></i>
                                </div>
                                <div class="col-xs-8 text-left">
                                    <span>Settings</span>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#" title="Settings">
                                <div class="col-xs-4">
                                    <i class="fa fa-question" aria-hidden="true"></i>
                                </div>
                                <div class="col-xs-8 text-left">
                                    <span>FAQ</span>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="./logout" title="Settings">
                                <div class="col-xs-4">
                                    <i class="fa fa-sign-out" aria-hidden="true"></i>
                                </div>
                                <div class="col-xs-8 text-left">
                                    <span>Logout</span>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="second-icon dropdown menu-icon">
                <span class="dropdown-toggle" role="button" id="dropdownNotification" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                    <span class="hidden-xs hidden-sm">Notifications</span> <i class="fa fa-bell-o" aria-hidden="true"></i> <span class="badge">0</span>
                </span>
                <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownNotification">
                    <!--                    <li class="new-not">
                                            <a href="#" title="User name comment"><img src="../resources/images/user2.jpg" alt="User name" class="img-circle img-user-mini"> User comments your post</a>
                                        </li>
                                        <li class="new-not">
                                            <a href="#" title="User name comment"><img src="../resources/images/user3.jpg" alt="User name" class="img-circle img-user-mini"> User comments your post</a>
                                        </li>
                                        <li>
                                            <a href="#" title="User name comment"><img src="../resources/images/user4.jpg" alt="User name" class="img-circle img-user-mini"> User comments your post</a>
                                        </li>
                                        <li role="separator" class="divider"></li>-->
                    <li><a href="#" title="All notifications">All Notifications</a></li>
                </ul>
            </div>
            <div class="second-icon menu-icon">
                <span><a href="#" title="Profile"><span class="hidden-xs hidden-sm">Profile</span> <i class="fa fa-user" aria-hidden="true"></i></a>
                </span>
            </div>
            <div class="second-icon menu-icon">
                <span><a href="#" title="Wall"><span class="hidden-xs hidden-sm">Feeds</span> <i class="fa fa-database" aria-hidden="true"></i></a>
                </span>
            </div>
        </header>

        <!--Left Sidebar with info Profile -->
        <div class="sidebar-nav">
            <a href="#" title="Profile">
                <img src="../resources/images/nationalGeographic_default_avatar_zj5qta.jpg" alt="User name" class="img-circle img-user">
            </a>
            <h2 class="text-center hidden-xs"><a href="#" title="Profile" id="profileName"></a></h2>
            <!--            <p class="text-center user-description hidden-xs">
                            <i>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</i>
                        </p>-->
        </div>

        <!--Wall with Post -->
        <div class="content-posts active" id="posts">
            <div id="posts-container" class="container-fluid container-posts">

                <!--                <div class="card-post">
                                    <div class="row">
                                        <div class="col-xs-3 col-sm-2">
                                            <a href="personal-profile.html" title="Profile">
                                                <img src="../resources/images/nationalGeographic_default_avatar_zj5qta.jpg" alt="User name" class="img-circle img-user">
                                            </a>
                                        </div>
                                        <div class="col-xs-9 col-sm-10 info-user">
                                            <h3><a href="personal-profile.html" title="Profile">My User</a></h3>
                                            <p><i>2h</i></p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-8 col-sm-offset-2 data-post">
                                            <p>Lorem Ipsum Dolor si amet</p>
                                            <div class="reaction">
                                                <span style="margin-right:1%;"><i class="fa fa-thumbs-o-up" style="font-size:170%;" id="like"></i><i class="fa fa-thumbs-o-up" style="color:blue;font-size:170%;display: none;" id="liked"></i></span> 156  <span style="margin-left:3%;"> <i class="fa fa-trash-o" style="font-size:170%;color:red;"></i></span>
                                            </div>
                                                                        <div class="comments">
                                                                            <div class="more-comments">View more comments</div>
                                                                            <ul>
                                                                                <li><b>User1</b> Lorem Ipsum Dolor si amet</li>
                                                                                <li><b>User2</b> Lorem Ipsum Dolor si amet &#x1F602;</li>
                                                                            </ul>
                                                                            <form>
                                                                                <input type="text" class="form-control" placeholder="Add a comment">
                                                                            </form>
                                                                        </div>
                                        </div>
                                    </div>
                                </div>-->

                <!--                <div class="card-post">
                                    <div class="row">
                                        <div class="col-xs-3 col-sm-2">
                                            <a href="user-profile.html" title="User Profile">
                                                <img src="../resources/images/user2.jpg" alt="User name" class="img-circle img-user">
                                            </a>
                                        </div>
                                        <div class="col-xs-9 col-sm-10 info-user">
                                            <h3><a href="user-profile.html" title="User Profile">User Name</a></h3>
                                            <p><i>2h</i></p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class=" col-sm-8 col-sm-offset-2 data-post">
                                            <p>Lorem Ipsum Dolor si amet</p>
                                            <img src="../resources/images/post.jpg" alt="image post" class="img-post">
                                            <div class="reaction">
                                                &#x2764; 1234 &#x1F603; 54
                                            </div>
                                            <div class="comments">
                                                <div class="more-comments">View more comments</div>
                                                <ul>
                                                    <li><b>User1</b> Lorem Ipsum Dolor si amet</li>
                                                    <li><b>User2</b> Lorem Ipsum Dolor si amet &#x1F602;</li>
                                                </ul>
                                                <form>
                                                    <input type="text" class="form-control" placeholder="Add a comment">
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>-->

                <!--                <div class="card-post">
                                    <div class="row">
                                        <div class="col-xs-3 col-sm-2">
                                            <a href="personal-profile.html" title="User Profile">
                                                <img src="../resources/images/user.jpg" alt="User name" class="img-circle img-user">
                                            </a>
                                        </div>
                                        <div class="col-xs-9 col-sm-10 info-user">
                                            <h3><a href="personal-profile.html" title="User Profile">My User</a></h3>
                                            <p><i>2h</i></p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-8 col-sm-offset-2 data-post">
                                            <p>Lorem Ipsum Dolor si amet</p>
                                            Video here
                                            <video controls>
                                                <source src="../resources/images/post-video.mp4" type="video/mp4">
                                                Your browser does not support the video tag.
                                            </video>
                                            <div class="reaction">
                                                &#x2764; 1234 &#x1F603; 54
                                            </div>
                                            <div class="comments">
                                                <div class="more-comments">View more comments</div>
                                                <ul>
                                                    <li><b>User1</b> Lorem Ipsum Dolor si amet</li>
                                                    <li><b>User2</b> Lorem Ipsum Dolor si amet &#x1F602;</li>
                                                </ul>
                                                <form>
                                                    <input type="text" class="form-control" placeholder="Add a comment">
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>-->
            </div>
            <!--Close #posts-container-->
            <!--            <div id="loading">
                            <img src="../resources/images/load.gif" alt="loader">
                        </div>-->
        </div>
        <!-- Close #posts -->
        <!-- Modal container for settings--->
        <!--        <div id="settingsmodal" class="modal fade text-center">
                    <div class="modal-dialog">
                        <div class="modal-content">
                        </div>
                    </div>
                </div>-->
        <script src="../resources/js/jquery-3.3.1.min.js"></script>
        <script src="../resources/js/bootstrap.min.js"></script>
        <script src="../resources/js/bootstrap-switch.min.js"></script>
        <script src="https://twemoji.maxcdn.com/twemoji.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
        <!--<script src="../resources/js/lazy-load.min.js"></script>-->
        <!--<script src="../resources/js/kudos.min.js"></script>-->
        <script type="text/javascript">
            $(document).ready(function () {
                loadProfileData();
                loadFeeds();
            });
            function loadProfileData() {
                $.ajax({
                    url: "./loadKudosUserData",
                    //                    dataType: "json",
                    contentType: 'application/json; charset=utf-8',
                    method: "Post",
                    async: false,
                    //                    data: JSON.stringify(postFilter),
                    success: function (jsonData) {
                        var trHTML = jsonData.firstName + ' ';
                        if (jsonData.middleName != "") {
                            trHTML += jsonData.middleName + ' ';
                        }
                        trHTML += jsonData.lastName;
                        $('#profileName').append(trHTML);
                    }
                });
            }
            ;
            function loadFeeds() {
                $.ajax({
                    url: "./loadAllKudosForFeeds",
                    //                    dataType: "json",
                    contentType: 'application/json; charset=utf-8',
                    method: "Post",
                    async: false,
                    //                    data: JSON.stringify(postFilter),
                    success: function (jsonData) {
                        var trHTML = '';
                        if (jsonData.status == 'success') {
                            if (jsonData.kudosPosts.length != 0) {
                                for (var i = 0; i < jsonData.kudosPosts.length; i++) {
                                    trHTML += '<div class="card-post" id=\"' + jsonData.kudosPosts[i].usersPostListId + '\">';
                                    trHTML += '<div class="row">';
                                    trHTML += '<div class="col-xs-3 col-sm-2">';
                                    trHTML += '<a href="#" title="Profile">';
                                    trHTML += '<img src="../resources/images/nationalGeographic_default_avatar_zj5qta.jpg" alt="User name" class="img-circle img-user">';
                                    trHTML += '</a>';
                                    trHTML += '</div>';
                                    trHTML += '<div class="col-xs-9 col-sm-10 info-user">';
                                    trHTML += '<h3><a href="#" title="Profile">';
                                    trHTML += jsonData.kudosPosts[i].firstName + " ";
                                    if (jsonData.kudosPosts[i].middleName != "") {
                                        trHTML += jsonData.kudosPosts[i].middleName + " ";
                                    }
                                    trHTML += jsonData.kudosPosts[i].lastName;
                                    trHTML += '</a></h3>';
                                    trHTML += '<p><i>' + jsonData.kudosPosts[i].createUtcStr + '</i></p>';
                                    trHTML += '</div>';
                                    trHTML += '</div>';
                                    trHTML += '<div class="row">';
                                    trHTML += '<div class="col-sm-8 col-sm-offset-2 data-post">';
                                    if (jsonData.kudosPosts[i].post != "") {
                                        trHTML += '<p>' + jsonData.kudosPosts[i].post + '</p>';
                                    }
                                    if (jsonData.kudosPosts[i].imageUrl != "") {
                                        trHTML += '<img src = \"' + jsonData.kudosPosts[i].imageUrl + '\" alt = "image post" class = "img-post">';
                                    }
                                    trHTML += '<div class="reaction">';
                                    trHTML += '<span style="margin-right:1%;">';
                                    if (jsonData.kudosPosts[i].isPostCreatedByUser == false && jsonData.kudosPosts[i].isPostLikedByUser == false) {
                                        trHTML += '<i class="fa fa-thumbs-o-up" style="font-size:170%;" id="like" onclick=\"likePost(' + jsonData.kudosPosts[i].usersPostListId + ')\"></i>';
                                    }
                                    if (jsonData.kudosPosts[i].isPostCreatedByUser == true) {
                                        trHTML += '<i class="fa fa-thumbs-o-up" style="font-size:170%;" id="like"></i>';
                                    }
                                    if (jsonData.kudosPosts[i].isPostLikedByUser == true) {
                                        trHTML += '<i class="fa fa-thumbs-o-up" style="color:blue;font-size:170%;" id="liked"></i>';
                                    }
                                    trHTML += '</span>';
                                    trHTML += jsonData.kudosPosts[i].totalLikeCount;
                                    trHTML += '<span style="margin-left:3%;">';
                                    if (jsonData.kudosPosts[i].isPostCreatedByUser == true) {
                                        trHTML += '<i class="fa fa-trash-o" style="font-size:170%;color:red;" onclick=\"deleteKudos(' + jsonData.kudosPosts[i].usersPostListId + ')\"></i>';
                                    }
                                    trHTML += '</span>';
                                    trHTML += '</div>';
                                    trHTML += '</div>';
                                    trHTML += '</div>';
                                    trHTML += '</div>';
                                }
                            }
                            $('#posts-container').append(trHTML);
                        }
                    }
                });
            }
            ;
            function likePost(id) {

                var data = {};
                data.usersPostListId = id;
                $.ajax({
                    url: "./likeAKudoCretedByAnyUser",
                    //                    dataType: "json",
                    contentType: 'application/json; charset=utf-8',
                    method: "Post",
                    async: false,
                    data: JSON.stringify(data),
                    success: function (jsonData) {
                        var trHTML = '';
                        if (jsonData.status == 'success') {
                            trHTML += '<div class="row">';
                            trHTML += '<div class="col-xs-3 col-sm-2">';
                            trHTML += '<a href="#" title="Profile">';
                            trHTML += '<img src="../resources/images/nationalGeographic_default_avatar_zj5qta.jpg" alt="User name" class="img-circle img-user">';
                            trHTML += '</a>';
                            trHTML += '</div>';
                            trHTML += '<div class="col-xs-9 col-sm-10 info-user">';
                            trHTML += '<h3><a href="#" title="Profile">';
                            trHTML += jsonData.firstName + " ";
                            if (jsonData.middleName != "") {
                                trHTML += jsonData.middleName + " ";
                            }
                            trHTML += jsonData.lastName;
                            trHTML += '</a></h3>';
                            trHTML += '<p><i>' + jsonData.createUtcStr + '</i></p>';
                            trHTML += '</div>';
                            trHTML += '</div>';
                            trHTML += '<div class="row">';
                            trHTML += '<div class="col-sm-8 col-sm-offset-2 data-post">';
                            if (jsonData.post != "") {
                                trHTML += '<p>' + jsonData.post + '</p>';
                            }
                            if (jsonData.imageUrl != "") {
                                trHTML += '<img src = \"' + jsonData.imageUrl + '\" alt = "image post" class = "img-post">';
                            }
                            trHTML += '<div class="reaction">';
                            trHTML += '<span style="margin-right:1%;">';
                            if (jsonData.isPostCreatedByUser == false && jsonData.isPostLikedByUser == false) {
                                trHTML += '<i class="fa fa-thumbs-o-up" style="font-size:170%;" id="like" onclick=\"likePost(' + jsonData.usersPostListId + ')\"></i>';
                            }
                            if (jsonData.isPostCreatedByUser == true) {
                                trHTML += '<i class="fa fa-thumbs-o-up" style="font-size:170%;" id="like"></i>';
                            }
                            if (jsonData.isPostLikedByUser == true) {
                                trHTML += '<i class="fa fa-thumbs-o-up" style="color:blue;font-size:170%;" id="liked"></i>';
                            }
                            trHTML += '</span>';
                            trHTML += jsonData.totalLikeCount;
                            trHTML += '<span style="margin-left:3%;">';
                            if (jsonData.isPostCreatedByUser == true) {
                                trHTML += '<i class="fa fa-trash-o" style="font-size:170%;color:red;" onclick=\"deleteKudos(' + jsonData.usersPostListId + ')\"></i>';
                            }
                            trHTML += '</span>';
                            trHTML += '</div>';
                            trHTML += '</div>';
                            trHTML += '</div>';


                            $('#' + jsonData.usersPostListId + '').empty();
                            $('#' + jsonData.usersPostListId + '').append(trHTML);
                        }
                    }
                });
            }
            ;
            function deleteKudos(id) {
                var jsonData = {};
                jsonData.usersPostListId = id;
                swal({
                    title: "Are you sure?",
                    text: "You want to delete this post.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, Delete it!",
                    cancelButtonText: "No, cancel please!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                        function (isConfirm) {
                            if (isConfirm) {
                                $.ajax({
                                    type: 'post',
                                    contentType: 'application/json; charset=utf-8',
                                    data: JSON.stringify(jsonData),
                                    async: false,
                                    url: "./deleteKudosById",
                                    success: function (data) {
                                        if (data.status === 'success') {
                                            swal({
                                                title: "Deleted!",
                                                type: "Success",
                                                showCancelButton: false,
                                                cancelButtonText: "ok",
                                                closeOnConfirm: false
                                            },
                                                    function (isConfirm) {
                                                        window.location = window.location.href;
                                                    });

                                        } else {
                                            swal("Error", "Unable to process at this moment)", "error");
                                        }
                                    }
                                });

                            } else {
                                swal("Cancelled", "Customer is safe now :", "error");
                            }
                        });
            }
            ;
        </script>
    </body>

</html>

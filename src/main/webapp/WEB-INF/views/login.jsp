
<!DOCTYPE html>
<html>

    <head>
        <title>Login || Kudos</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="UTF-8">
        <link rel="stylesheet" href="./resources/css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="./resources/css/font-awesome.min.css">
        <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,600,700" rel="stylesheet">
        <link rel="stylesheet" href="./resources/css/style.css" type="text/css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
    <body id="login-page">

        <div class="form-box formdatalogin">
            <div class="form-logo">
                <p class="text-center"><img src="./resources/images/logo.png" alt="logo"></p>
            </div>
            <div class="form-group servermessagelogin">
            </div>
            <!--            <form>-->
            <input type="text" class="form-control" placeholder="Username" id="username" name="username">
            <input type="password" class="form-control" placeholder="Password" id="password" name="password">
            <input type="checkbox"> Save Password
            <!--<input type="button" class="btn btn-social" value="LOGIN">-->
            <button type="submit" class="btn btn-social" onclick="usersLogin()">LOGIN<span style="float:right;display: none;" id="loginSpinner"><i class="fa fa-spinner fa-pulse fa-1x fa-fw" style="color:white;"></i></span></button>
            <!--</form>-->
            <a href="#" class="login-action" title="Forgot password">
                <p>Forgot your password?</p>
            </a>
            <a href="#" class="login-action" title="Sign in">
                <p>New User?</p>
            </a>
        </div>
        <!--Close form-box-->
        <div class="errormessage" style="display: none;">
            <div class="alert alert-danger fade in">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                [[message]]
            </div>
        </div>

        <div class="successmessage" style="display: none;">
            <div class="alert alert-success fade in">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                [[message]]
            </div>
        </div>
        <footer class="login-footer text-center">
            <div class="col-xs-3">
                <a class="login-action" href="#" title="language">LANGUAGE</a>
            </div>
            <div class="col-xs-3">
                <a class="login-action" href="#" title="terms">TERMS</a>
            </div>
            <div class="col-xs-3">
                <a class="login-action" href="#" title="privacy">PRIVACY</a>
            </div>
            <div class="col-xs-3">
                <a class="login-action" href="#" title="careers">CAREERS</a>
            </div>
        </footer>
        <script src="./resources/js/jquery-3.2.0.min.js"></script>
        <script src="./resources/js/bootstrap.min.js"></script>
        <style type="text/css">
            .error-input{
                border-color: #a94442;
                -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
                box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
            }
            .hpanel.hgreen .panel-body {
                border-top: 2px solid #1c3659;
            }
            .shadow{
                -webkit-box-shadow: 8px 10px 35px 5px rgba(0,0,0,0.75);
                -moz-box-shadow: 8px 10px 35px 5px rgba(0,0,0,0.75);
                box-shadow: 8px 10px 35px 5px rgba(0,0,0,0.75);
            }
            select:focus{
                border-color:black;
                -moz-box-shadow: 0px 0px 6px #dfdfdf;
                -webkit-box-shadow: 0px 0px 6px #dfdfdf;
                /*box-shadow: 0px 0px 6px #dfdfdf;*/
                border: 1px solid rgba(81, 203, 238, 1);
                box-shadow: 0 0 5px rgba(81, 203, 238, 1);
            }
            input[type=text]:focus, input[type=password]:focus{
                border-color:black;
                -moz-box-shadow: 0px 0px 6px #dfdfdf;
                -webkit-box-shadow: 0px 0px 6px #dfdfdf;
                /*box-shadow: 0px 0px 6px #dfdfdf;*/
                border: 1px solid rgba(81, 203, 238, 1);
                box-shadow: 0 0 5px rgba(81, 203, 238, 1);
            }
        </style>
        <script>
                function checkBlank(jqDomArr) {
                    var errorFlag = false;
                    $.each(jqDomArr, function (index, value) {
                        var domObj = value;
                        if (domObj.val() == "" || domObj.val() == "0") {
                            domObj.parent().addClass("has-error");
                            errorFlag = true;
                        } else {
                            domObj.parent().removeClass("has-error");
                        }
                    });
                    return errorFlag;
                }
                function usersLogin() {
                    $('#loginSpinner').show();
                    var jqDomArr = [
                        $(".formdatalogin").find("input[name='username']"),
                        $(".formdatalogin").find("input[name='password']")
                    ];
                    var error = checkBlank(jqDomArr);
                    if (!error) {
                        //  $('#loginSpinner').show();
                        document.getElementById('loginSpinner').style.display = 'block';
                        var jsonData = {};
                        jsonData.userName = $(".formdatalogin").find("input[name='username']").val();
                        jsonData.password = $(".formdatalogin").find("input[name='password']").val();
                        login(jsonData)
                    } else {
                        var strVar = "";
                        strVar += "<div class=\"alert alert-danger fade in\">";
                        strVar += "    <a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\"  style=\"margin-top:-1.5%\">&times;<\/a>";
                        strVar += "    <strong>Error!<\/strong> Please fill required fields.";
                        strVar += "  <\/div>";
                        $(".servermessagelogin").html(strVar);
                        $('#loginSpinner').hide();
                    }
                }
                function login(jsonData) {

                    $.ajax({
                        type: 'post',
                        contentType: 'application/json; charset=utf-8',
                        data: JSON.stringify(jsonData),
                        async: false,
                        url: "./userslogin",
                        success: function (data) {
                            // alert(data.status);
                            if (data.status === 'error') {
                                var message = $(".errormessage").html();
                                message = message.replace("[[message]]", data.message);
                                $(".servermessagelogin").html(message);
                                document.getElementById('loginSpinner').style.display = 'none';
                            } else {
                                document.getElementById('loginSpinner').style.display = 'none';
                                window.location = "./kudosusers/feeds";
                            }
                        }
                    });
                }
        </script>
    </body>

</html>

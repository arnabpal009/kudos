/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.kudos.interceptor;

import in.kudos.domain.Users;
import in.kudos.util.KudosConstant;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 *
 * @author @rn@b
 */
public class UsersSessionInterseptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        HttpSession session = request.getSession();
        Users users = (Users) session.getAttribute(KudosConstant.KUDOSUSERS);
        if (users == null) {

            String context = request.getServletContext().getContextPath();

            response.sendRedirect(context + "/");
            return false;
        }
        return true;
    }

}

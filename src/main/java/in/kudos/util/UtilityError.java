package in.kudos.util;

/**
 * @author:Tarasankar Panda
 * @Project : ChefSnapp App
 * @version :1.0
 * @Company :WWW.CLOUDLABZ.IN
 * @date :24th March, 2014
 */
public class UtilityError extends RuntimeException {

    private static final long serialVersionUID = 159337L;

    public UtilityError(String message) {
        super("ERROR " + message);
    }
}

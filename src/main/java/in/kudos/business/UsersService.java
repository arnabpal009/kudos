/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.kudos.business;

import in.kudos.domain.Users;
import in.kudos.dto.UsersBean;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * @author @rn@b
 */
public interface UsersService {

    public UsersBean checkUsersCredentialForLogin(UsersBean usersBean, HttpSession session, HttpServletRequest request);

    public UsersBean loadKudosUserData(Users users);

    public UsersBean deleteKudosById(Users users, UsersBean usersBean);

    public UsersBean likeAKudoCretedByAnyUser(Users users, UsersBean usersBean);

    public UsersBean loadAllKudosForFeeds(Users users);

}

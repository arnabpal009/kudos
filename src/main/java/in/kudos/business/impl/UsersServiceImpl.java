/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.kudos.business.impl;

import in.kudos.business.UsersService;
import in.kudos.dao.UsersDao;
import in.kudos.dao.UsersLoginCredentialsDao;
import in.kudos.dao.UsersPostKudosDao;
import in.kudos.dao.UsersPostListDao;
import in.kudos.domain.Users;
import in.kudos.domain.UsersLoginCredentials;
import in.kudos.domain.UsersPostKudos;
import in.kudos.domain.UsersPostList;
import in.kudos.dto.UsersBean;
import in.kudos.util.KudosConstant;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author @rn@b
 */
public class UsersServiceImpl implements UsersService {

    @Autowired
    UsersDao usersDao;
    @Autowired
    UsersLoginCredentialsDao usersLoginCredentialsDao;
    @Autowired
    UsersPostListDao usersPostListDao;
    @Autowired
    UsersPostKudosDao usersPostKudosDao;

    @Override
    public UsersBean checkUsersCredentialForLogin(UsersBean usersBean, HttpSession session, HttpServletRequest request) {

        SimpleDateFormat sdtf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm a");
        UsersBean bean = new UsersBean();
        if (usersBean != null) {
            UsersLoginCredentials ulc = new UsersLoginCredentials();
//            System.out.println("the username is " + usersBean.getUserName());
            List<UsersLoginCredentials> list = usersLoginCredentialsDao.checkUsersByUName(usersBean.getUserName().trim());
//            System.out.println("the size of the bean is " + list.size());
            if (list.size() != 0) {
                if (list.get(0).getPassword().equals(usersBean.getPassword()) && list.get(0).getUserId().getIsProfileBlocked() == Boolean.FALSE && list.get(0).getUserId().getIsProfileDeleted() == Boolean.FALSE && (list.get(0).getUserId().getPhoneVerifyStatus() == Boolean.TRUE || list.get(0).getUserId().getEmailVerifyStatus() == Boolean.TRUE)) {

                    list.get(0).setLastLoginUtc(new Date());
                    ulc = usersLoginCredentialsDao.save(list.get(0));
                    session.invalidate();
                    HttpSession newSession = request.getSession(true);
                    newSession.setAttribute(KudosConstant.KUDOSUSERS, ulc.getUserId());
                    bean.setMessage(KudosConstant.KUDOSUSERS);
                    bean.setStatus(KudosConstant.SUCCESS);

                } else if (list.get(0).getUserId().getIsProfileBlocked() == Boolean.TRUE) {
                    bean.setMessage("Your profile is blocked. Please Contact admin.");
                    bean.setStatus(KudosConstant.ERROR);
                } else if (list.get(0).getUserId().getIsProfileDeleted() == Boolean.TRUE) {
                    bean.setMessage("Your profile has being deleted. Please create a profile.");
                    bean.setStatus(KudosConstant.ERROR);
                } else if (list.get(0).getUserId().getPhoneVerifyStatus() == Boolean.FALSE && list.get(0).getUserId().getEmailVerifyStatus() == Boolean.FALSE) {
                    if (list.get(0).getUserId().getPhoneVerifyStatus() == Boolean.FALSE) {
                        bean.setMessage("Please verify your phone no. to login.");
                        bean.setStatus(KudosConstant.ERROR);
                    } else {
                        bean.setMessage("Please verify your email id to login.");
                        bean.setStatus(KudosConstant.ERROR);
                    }
                } else {
                    bean.setMessage("You have provided Wrong Credential.");
                    bean.setStatus(KudosConstant.ERROR);
                }
            } else {
                bean.setMessage("Your profile is not registered with us.");
                bean.setStatus(KudosConstant.ERROR);
            }
        } else {
            bean.setMessage("You are not authorize to access. Please try again later.");
            bean.setStatus(KudosConstant.ERROR);
        }
        return bean;

    }

    @Override
    public UsersBean loadKudosUserData(Users users) {
        UsersBean bean = new UsersBean();
        if (users != null) {
            bean.setUsersId(users.getId());
            bean.setFirstName(users.getFirstName());
            bean.setLastName(users.getLastName());
            if (users.getMiddleName() != null && !users.getMiddleName().equals("")) {
                bean.setMiddleName(users.getMiddleName());
            } else {
                bean.setMiddleName("");
            }
            if (users.getNickName() != null && !users.getNickName().equals("")) {
                bean.setNickName(users.getNickName());
            } else {
                bean.setNickName("");
            }
            bean.setStatus(KudosConstant.SUCCESS);
            bean.setMessage("Profile information loaded successfully.");
        } else {
            bean.setStatus(KudosConstant.ERROR);
            bean.setMessage("You are not authorized to access! Please login.");
        }
        return bean;
    }

    @Override
    public UsersBean deleteKudosById(Users users, UsersBean usersBean) {
        UsersBean bean = new UsersBean();
        if (users != null && usersBean.getUsersPostListId() != null) {
            UsersPostList upl = usersPostListDao.loadById(usersBean.getUsersPostListId());
            if (upl != null && upl.getUserId().getId() == users.getId()) {
                usersPostListDao.delete(upl);
                bean.setStatus(KudosConstant.SUCCESS);
                bean.setMessage("You have deleted your kudos successfully");
            } else if (upl.getUserId() != users) {
                bean.setStatus(KudosConstant.ERROR);
                bean.setMessage("This kudos is not created by you");
            } else {
                bean.setStatus(KudosConstant.ERROR);
                bean.setMessage("Opps! Something went wrong. Please try again later.");
            }
        } else {
            bean.setStatus(KudosConstant.ERROR);
            bean.setMessage("You are not authorized to access! Please login.");
        }
        return bean;
    }

    @Override
    public UsersBean likeAKudoCretedByAnyUser(Users users, UsersBean usersBean) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm a");
        UsersBean bean = new UsersBean();
        if (users != null && usersBean.getUsersPostListId() != null) {
            UsersPostList upl = usersPostListDao.loadById(usersBean.getUsersPostListId());
            if (upl != null && upl.getUserId() != users) {
                UsersPostKudos upks = new UsersPostKudos();
                upks.setUserId(users);
                upks.setUserPostId(upl);
                upks.setCreateUtc(new Date());
                upks.setUpdateUtc(new Date());
                usersPostKudosDao.save(upks);
                bean.setUsersPostListId(upl.getId());
                bean.setUsersId(upl.getUserId().getId());
                bean.setFirstName(upl.getUserId().getFirstName());
                bean.setLastName(upl.getUserId().getLastName());
                if (upl.getUserId().getMiddleName() != null && !upl.getUserId().getMiddleName().equals("")) {
                    bean.setMiddleName(upl.getUserId().getMiddleName());
                } else {
                    bean.setMiddleName("");
                }
                if (upl.getPost() != null && !upl.getPost().equals("")) {
                    bean.setPost(upl.getPost());
                } else {
                    bean.setPost("");
                }
                if (upl.getImageUrl() != null && !upl.getImageUrl().equals("")) {
                    bean.setImageUrl(upl.getImageUrl());
                } else {
                    bean.setImageUrl("");
                }
                bean.setCreateUtcStr(sdf.format(upl.getCreateUtc()));
                System.out.println(" the list id is " + upl.getUserId().getId() + " the user id is " + users.getId());
                if (upl.getUserId().getId() == users.getId()) {
                    bean.setIsPostCreatedByUser(Boolean.TRUE);
                    bean.setIsPostLikedByUser(Boolean.FALSE);
                } else {
                    bean.setIsPostCreatedByUser(Boolean.FALSE);
                    List<UsersPostKudos> l = usersPostKudosDao.getUserLikedPostByUserId(users, upl);
                    if (l.size() != 0) {
                        bean.setIsPostLikedByUser(Boolean.TRUE);
                    } else {
                        bean.setIsPostLikedByUser(Boolean.FALSE);
                    }
                }
                List<UsersPostKudos> l = usersPostKudosDao.getTotalLikesForAKudos(upl);
                bean.setTotalLikeCount(l.size());
                bean.setStatus(KudosConstant.SUCCESS);
                bean.setMessage("You liked the kudos.");
            } else if (upl.getUserId() == users) {
                bean.setStatus(KudosConstant.ERROR);
                bean.setMessage("This kudos is created by you");
            } else {
                bean.setStatus(KudosConstant.ERROR);
                bean.setMessage("Opps! Something went wrong. Please try again later.");
            }
        } else {
            bean.setStatus(KudosConstant.ERROR);
            bean.setMessage("You are not authorized to access! Please login.");
        }
        return bean;

    }

    @Override
    public UsersBean loadAllKudosForFeeds(Users users) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm a");
        UsersBean bean = new UsersBean();
        List<UsersBean> beans = new ArrayList<UsersBean>();
        if (users != null) {
            List<UsersPostList> upl = usersPostListDao.loadAllKudosInSorted();
            if (upl.size() != 0) {
                for (UsersPostList list : upl) {
                    UsersBean ub = new UsersBean();
                    ub.setUsersPostListId(list.getId());
                    ub.setUsersId(list.getUserId().getId());
                    ub.setFirstName(list.getUserId().getFirstName());
                    ub.setLastName(list.getUserId().getLastName());
                    if (list.getUserId().getMiddleName() != null && !list.getUserId().getMiddleName().equals("")) {
                        ub.setMiddleName(list.getUserId().getMiddleName());
                    } else {
                        ub.setMiddleName("");
                    }
                    if (list.getPost() != null && !list.getPost().equals("")) {
                        ub.setPost(list.getPost());
                    } else {
                        ub.setPost("");
                    }
                    if (list.getImageUrl() != null && !list.getImageUrl().equals("")) {
                        ub.setImageUrl(list.getImageUrl());
                    } else {
                        ub.setImageUrl("");
                    }
                    ub.setCreateUtcStr(sdf.format(list.getCreateUtc()));
                    System.out.println(" the list id is " + list.getUserId().getId() + " the user id is " + users.getId());
                    if (list.getUserId().getId() == users.getId()) {
                        ub.setIsPostCreatedByUser(Boolean.TRUE);
                        ub.setIsPostLikedByUser(Boolean.FALSE);
                    } else {
                        ub.setIsPostCreatedByUser(Boolean.FALSE);
                        List<UsersPostKudos> l = usersPostKudosDao.getUserLikedPostByUserId(users, list);
                        if (l.size() != 0) {
                            ub.setIsPostLikedByUser(Boolean.TRUE);
                        } else {
                            ub.setIsPostLikedByUser(Boolean.FALSE);
                        }
                    }
                    List<UsersPostKudos> l = usersPostKudosDao.getTotalLikesForAKudos(list);
                    ub.setTotalLikeCount(l.size());
                    beans.add(ub);
                }
                bean.setKudosPosts(beans);
                bean.setStatus(KudosConstant.SUCCESS);
                bean.setMessage("All kudos loaded successfully.");
            } else {
                bean.setStatus(KudosConstant.ERROR);
                bean.setMessage("Opps! No Kudos found to be loaded");
            }
        } else {
            bean.setStatus(KudosConstant.ERROR);
            bean.setMessage("You are not authorized to access! Please login.");
        }
        return bean;

    }

}

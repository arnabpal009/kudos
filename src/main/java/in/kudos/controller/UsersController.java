/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.kudos.controller;

import in.kudos.business.UsersService;
import in.kudos.domain.Users;
import in.kudos.dto.UsersBean;
import in.kudos.util.KudosConstant;
import java.text.ParseException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author @rn@b
 */
@Controller
@RequestMapping("/kudosusers/**")
public class UsersController {

    @Autowired
    UsersService usersService;

    //index page for kudos
    @RequestMapping(value = "/feeds", method = RequestMethod.GET)
    public String indexPage(ModelMap modelMap) {
        return "feeds";
    }

    //loading of user information
    @RequestMapping(value = "/loadKudosUserData", method = RequestMethod.POST)
    public @ResponseBody
    UsersBean loadKudosUserData(HttpServletRequest request, HttpSession session) throws ParseException {
        UsersBean bean = new UsersBean();
        Users users = (Users) session.getAttribute(KudosConstant.KUDOSUSERS);
        if (users != null) {
            bean = usersService.loadKudosUserData(users);
        } else {
            bean.setStatus(KudosConstant.ERROR);
            bean.setMessage("You are not authorized to access! Please login.");
        }
        return bean;
    }

    //delete own kudos of user by the user
    @RequestMapping(value = "/deleteKudosById", method = RequestMethod.POST)
    public @ResponseBody
    UsersBean deleteKudosById(@RequestBody UsersBean usersBean, HttpServletRequest request, HttpSession session) throws ParseException {
        UsersBean bean = new UsersBean();
        Users users = (Users) session.getAttribute(KudosConstant.KUDOSUSERS);
        if (users != null) {
            bean = usersService.deleteKudosById(users, usersBean);
        } else {
            bean.setStatus(KudosConstant.ERROR);
            bean.setMessage("You are not authorized to access! Please login.");
        }
        return bean;
    }

    //like kudos created by any other user
    @RequestMapping(value = "/likeAKudoCretedByAnyUser", method = RequestMethod.POST)
    public @ResponseBody
    UsersBean likeAKudoCretedByAnyUser(@RequestBody UsersBean usersBean, HttpServletRequest request, HttpSession session) throws ParseException {
        UsersBean bean = new UsersBean();
        Users users = (Users) session.getAttribute(KudosConstant.KUDOSUSERS);
        if (users != null) {
            bean = usersService.likeAKudoCretedByAnyUser(users, usersBean);
        } else {
            bean.setStatus(KudosConstant.ERROR);
            bean.setMessage("You are not authorized to access! Please login.");
        }
        return bean;
    }

    //load all kudos for feeds with all info
    @RequestMapping(value = "/loadAllKudosForFeeds", method = RequestMethod.POST)
    public @ResponseBody
    UsersBean loadAllKudosForFeeds(HttpServletRequest request, HttpSession session) throws ParseException {
        UsersBean bean = new UsersBean();
        Users users = (Users) session.getAttribute(KudosConstant.KUDOSUSERS);
        if (users != null) {
            bean = usersService.loadAllKudosForFeeds(users);
        } else {
            bean.setStatus(KudosConstant.ERROR);
            bean.setMessage("You are not authorized to access! Please login.");
        }
        return bean;
    }

    @RequestMapping(value = "/logout")
    public String logOut(HttpSession session) {
        session.invalidate();
        return "redirect:../";
    }
}

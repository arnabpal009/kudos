/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.kudos.controller;

import in.kudos.business.UsersService;
import in.kudos.dto.UsersBean;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import oracle.jrockit.jfr.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author @rn@b
 */
@Controller
public class HomeController {

    @Autowired
    UsersService usersService;

    //index page for kudos
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String indexPage(ModelMap modelMap) {
        return "index";
    }

    //Customer Login
    @RequestMapping(value = "/userslogin", method = RequestMethod.POST)
    public @ResponseBody
    UsersBean customerLogin(@RequestBody UsersBean usersBean, HttpSession session, HttpServletRequest request) throws ParseException {
        //System.out.println("com.jihuzur.controller.CustomerHomeController.customerLogin()");
        UsersBean bean = usersService.checkUsersCredentialForLogin(usersBean, session, request);
        return bean;
    }

}

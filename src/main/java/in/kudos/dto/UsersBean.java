/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.kudos.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.util.Date;
import java.util.List;

/**
 *
 * @author @rn@b
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class UsersBean {

    private String status;
    private String message;
    private String userName;
    private String password;
    private Long usersId;
    private String firstName;
    private String middleName;
    private String lastName;
    private String nickName;
    private Long usersPostListId;
    private String post;
    private String imageUrl;
    private Date createUtc;
    private String createUtcStr;
    private Date updateUtc;
    private String updateUtcStr;
    private Integer totalLikeCount;
    private Boolean isPostCreatedByUser;
    private Boolean isPostLikedByUser;
    private List<UsersBean> kudosPosts;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getUsersId() {
        return usersId;
    }

    public void setUsersId(Long usersId) {
        this.usersId = usersId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public Long getUsersPostListId() {
        return usersPostListId;
    }

    public void setUsersPostListId(Long usersPostListId) {
        this.usersPostListId = usersPostListId;
    }

    public List<UsersBean> getKudosPosts() {
        return kudosPosts;
    }

    public void setKudosPosts(List<UsersBean> kudosPosts) {
        this.kudosPosts = kudosPosts;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Date getCreateUtc() {
        return createUtc;
    }

    public void setCreateUtc(Date createUtc) {
        this.createUtc = createUtc;
    }

    public String getCreateUtcStr() {
        return createUtcStr;
    }

    public void setCreateUtcStr(String createUtcStr) {
        this.createUtcStr = createUtcStr;
    }

    public Date getUpdateUtc() {
        return updateUtc;
    }

    public void setUpdateUtc(Date updateUtc) {
        this.updateUtc = updateUtc;
    }

    public String getUpdateUtcStr() {
        return updateUtcStr;
    }

    public void setUpdateUtcStr(String updateUtcStr) {
        this.updateUtcStr = updateUtcStr;
    }

    public Integer getTotalLikeCount() {
        return totalLikeCount;
    }

    public void setTotalLikeCount(Integer totalLikeCount) {
        this.totalLikeCount = totalLikeCount;
    }

    public Boolean getIsPostCreatedByUser() {
        return isPostCreatedByUser;
    }

    public void setIsPostCreatedByUser(Boolean isPostCreatedByUser) {
        this.isPostCreatedByUser = isPostCreatedByUser;
    }

    public Boolean getIsPostLikedByUser() {
        return isPostLikedByUser;
    }

    public void setIsPostLikedByUser(Boolean isPostLikedByUser) {
        this.isPostLikedByUser = isPostLikedByUser;
    }

}

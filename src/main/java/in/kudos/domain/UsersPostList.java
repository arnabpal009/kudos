/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.kudos.domain;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author @rn@b
 */
@Entity
@Table(name = "users_post_list")

public class UsersPostList implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Long id;

    @Lob
    @Size(max = 2147483647)
    @Column(name = "post")
    private String post;

    @Size(max = 255)
    @Column(name = "image_url")
    private String imageUrl;

    @Basic(optional = false)
    @NotNull
    @Column(name = "create_utc")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createUtc;

    @Column(name = "update_utc")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateUtc;

    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Users userId;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userPostId")
    private Collection<UsersPostKudos> usersPostKudosCollection;

    public UsersPostList() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Date getCreateUtc() {
        return createUtc;
    }

    public void setCreateUtc(Date createUtc) {
        this.createUtc = createUtc;
    }

    public Date getUpdateUtc() {
        return updateUtc;
    }

    public void setUpdateUtc(Date updateUtc) {
        this.updateUtc = updateUtc;
    }

    public Users getUserId() {
        return userId;
    }

    public void setUserId(Users userId) {
        this.userId = userId;
    }

    public Collection<UsersPostKudos> getUsersPostKudosCollection() {
        return usersPostKudosCollection;
    }

    public void setUsersPostKudosCollection(Collection<UsersPostKudos> usersPostKudosCollection) {
        this.usersPostKudosCollection = usersPostKudosCollection;
    }

    @SuppressWarnings({"JpaModelErrorInspection"})
    public boolean isNew() {
        return (id == null);
    }

}

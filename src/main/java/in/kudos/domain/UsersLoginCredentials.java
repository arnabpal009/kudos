/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.kudos.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author @rn@b
 */
@Entity
@Table(name = "users_login_credentials")

public class UsersLoginCredentials implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Long id;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "user_name")
    private String userName;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "password")
    private String password;

    @Column(name = "last_login_utc")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastLoginUtc;

    @Column(name = "last_logout_utc")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastLogoutUtc;

    @Basic(optional = false)
    @NotNull
    @Column(name = "create_utc")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createUtc;

    @Column(name = "update_utc")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateUtc;

    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Users userId;

    public UsersLoginCredentials() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getLastLoginUtc() {
        return lastLoginUtc;
    }

    public void setLastLoginUtc(Date lastLoginUtc) {
        this.lastLoginUtc = lastLoginUtc;
    }

    public Date getLastLogoutUtc() {
        return lastLogoutUtc;
    }

    public void setLastLogoutUtc(Date lastLogoutUtc) {
        this.lastLogoutUtc = lastLogoutUtc;
    }

    public Date getCreateUtc() {
        return createUtc;
    }

    public void setCreateUtc(Date createUtc) {
        this.createUtc = createUtc;
    }

    public Date getUpdateUtc() {
        return updateUtc;
    }

    public void setUpdateUtc(Date updateUtc) {
        this.updateUtc = updateUtc;
    }

    public Users getUserId() {
        return userId;
    }

    public void setUserId(Users userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "in.kudos.domain.UsersLoginCredentials[ id=" + id + " ]";
    }

}

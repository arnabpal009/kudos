/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.kudos.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author @rn@b
 */
@Entity
@Table(name = "users_friend_list")

public class UsersFriendList implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Long id;

    @Basic(optional = false)
    @NotNull
    @Column(name = "is_accepted")
    private Boolean isAccepted;

    @Column(name = "accepted_utc")
    @Temporal(TemporalType.TIMESTAMP)
    private Date acceptedUtc;

    @Basic(optional = false)
    @NotNull
    @Column(name = "is_deleted_by_from")
    private Boolean isDeletedByFrom;

    @Column(name = "is_deleted_by_from_utc")
    @Temporal(TemporalType.TIMESTAMP)
    private Date isDeletedByFromUtc;

    @Basic(optional = false)
    @NotNull
    @Column(name = "is_deleted_by_to")
    private Boolean isDeletedByTo;

    @Column(name = "is_deleted_by_to_utc")
    @Temporal(TemporalType.TIMESTAMP)
    private Date isDeletedByToUtc;

    @Basic(optional = false)
    @NotNull
    @Column(name = "create_utc")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createUtc;

    @Column(name = "update_utc")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateUtc;

    @JoinColumn(name = "from__user_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Users fromUserId;

    @JoinColumn(name = "to_user_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Users toUserId;

    public UsersFriendList() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getIsAccepted() {
        return isAccepted;
    }

    public void setIsAccepted(Boolean isAccepted) {
        this.isAccepted = isAccepted;
    }

    public Date getAcceptedUtc() {
        return acceptedUtc;
    }

    public void setAcceptedUtc(Date acceptedUtc) {
        this.acceptedUtc = acceptedUtc;
    }

    public Boolean getIsDeletedByFrom() {
        return isDeletedByFrom;
    }

    public void setIsDeletedByFrom(Boolean isDeletedByFrom) {
        this.isDeletedByFrom = isDeletedByFrom;
    }

    public Date getIsDeletedByFromUtc() {
        return isDeletedByFromUtc;
    }

    public void setIsDeletedByFromUtc(Date isDeletedByFromUtc) {
        this.isDeletedByFromUtc = isDeletedByFromUtc;
    }

    public Boolean getIsDeletedByTo() {
        return isDeletedByTo;
    }

    public void setIsDeletedByTo(Boolean isDeletedByTo) {
        this.isDeletedByTo = isDeletedByTo;
    }

    public Date getIsDeletedByToUtc() {
        return isDeletedByToUtc;
    }

    public void setIsDeletedByToUtc(Date isDeletedByToUtc) {
        this.isDeletedByToUtc = isDeletedByToUtc;
    }

    public Date getCreateUtc() {
        return createUtc;
    }

    public void setCreateUtc(Date createUtc) {
        this.createUtc = createUtc;
    }

    public Date getUpdateUtc() {
        return updateUtc;
    }

    public void setUpdateUtc(Date updateUtc) {
        this.updateUtc = updateUtc;
    }

    public Users getFromUserId() {
        return fromUserId;
    }

    public void setFromUserId(Users fromUserId) {
        this.fromUserId = fromUserId;
    }

    public Users getToUserId() {
        return toUserId;
    }

    public void setToUserId(Users toUserId) {
        this.toUserId = toUserId;
    }

    @SuppressWarnings({"JpaModelErrorInspection"})
    public boolean isNew() {
        return (id == null);
    }

}

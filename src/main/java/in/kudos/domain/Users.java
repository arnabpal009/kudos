/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.kudos.domain;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author @rn@b
 */
@Entity
@Table(name = "users")

public class Users implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Long id;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "first_name")
    private String firstName;

    @Size(max = 255)
    @Column(name = "middle_name")
    private String middleName;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "last_name")
    private String lastName;

    @Size(max = 255)
    @Column(name = "nick_name")
    private String nickName;

    @Size(max = 255)
    @Column(name = "phone_number")
    private String phoneNumber;

    @Size(max = 255)
    @Column(name = "phone_verify_code")
    private String phoneVerifyCode;

    @Size(max = 255)
    @Column(name = "email_id")
    private String emailId;

    @Size(max = 255)
    @Column(name = "email_verify_code")
    private String emailVerifyCode;

    @Column(name = "phone_verify_status")
    private Boolean phoneVerifyStatus;

    @Column(name = "email_verify_status")
    private Boolean emailVerifyStatus;

    @Basic(optional = false)
    @NotNull
    @Column(name = "is_profile_deleted")
    private Boolean isProfileDeleted;

    @Basic(optional = false)
    @NotNull
    @Column(name = "is_profile_blocked")
    private Boolean isProfileBlocked;

    @Basic(optional = false)
    @NotNull
    @Column(name = "create_utc")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createUtc;

    @Column(name = "update_utc")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateUtc;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId")
    private Collection<UsersLoginCredentials> usersLoginCredentialsCollection;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId")
    private Collection<UsersPostList> usersPostListCollection;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId")
    private Collection<UsersPostKudos> usersPostKudosCollection;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fromUserId")
    private Collection<UsersFriendList> usersFriendListCollection;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "toUserId")
    private Collection<UsersFriendList> usersFriendListCollection1;

    public Users() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneVerifyCode() {
        return phoneVerifyCode;
    }

    public void setPhoneVerifyCode(String phoneVerifyCode) {
        this.phoneVerifyCode = phoneVerifyCode;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getEmailVerifyCode() {
        return emailVerifyCode;
    }

    public void setEmailVerifyCode(String emailVerifyCode) {
        this.emailVerifyCode = emailVerifyCode;
    }

    public Boolean getPhoneVerifyStatus() {
        return phoneVerifyStatus;
    }

    public void setPhoneVerifyStatus(Boolean phoneVerifyStatus) {
        this.phoneVerifyStatus = phoneVerifyStatus;
    }

    public Boolean getEmailVerifyStatus() {
        return emailVerifyStatus;
    }

    public void setEmailVerifyStatus(Boolean emailVerifyStatus) {
        this.emailVerifyStatus = emailVerifyStatus;
    }

    public Boolean getIsProfileDeleted() {
        return isProfileDeleted;
    }

    public void setIsProfileDeleted(Boolean isProfileDeleted) {
        this.isProfileDeleted = isProfileDeleted;
    }

    public Boolean getIsProfileBlocked() {
        return isProfileBlocked;
    }

    public void setIsProfileBlocked(Boolean isProfileBlocked) {
        this.isProfileBlocked = isProfileBlocked;
    }

    public Date getCreateUtc() {
        return createUtc;
    }

    public void setCreateUtc(Date createUtc) {
        this.createUtc = createUtc;
    }

    public Date getUpdateUtc() {
        return updateUtc;
    }

    public void setUpdateUtc(Date updateUtc) {
        this.updateUtc = updateUtc;
    }

    public Collection<UsersLoginCredentials> getUsersLoginCredentialsCollection() {
        return usersLoginCredentialsCollection;
    }

    public void setUsersLoginCredentialsCollection(Collection<UsersLoginCredentials> usersLoginCredentialsCollection) {
        this.usersLoginCredentialsCollection = usersLoginCredentialsCollection;
    }

    public Collection<UsersPostList> getUsersPostListCollection() {
        return usersPostListCollection;
    }

    public void setUsersPostListCollection(Collection<UsersPostList> usersPostListCollection) {
        this.usersPostListCollection = usersPostListCollection;
    }

    public Collection<UsersPostKudos> getUsersPostKudosCollection() {
        return usersPostKudosCollection;
    }

    public void setUsersPostKudosCollection(Collection<UsersPostKudos> usersPostKudosCollection) {
        this.usersPostKudosCollection = usersPostKudosCollection;
    }

    public Collection<UsersFriendList> getUsersFriendListCollection() {
        return usersFriendListCollection;
    }

    public void setUsersFriendListCollection(Collection<UsersFriendList> usersFriendListCollection) {
        this.usersFriendListCollection = usersFriendListCollection;
    }

    public Collection<UsersFriendList> getUsersFriendListCollection1() {
        return usersFriendListCollection1;
    }

    public void setUsersFriendListCollection1(Collection<UsersFriendList> usersFriendListCollection1) {
        this.usersFriendListCollection1 = usersFriendListCollection1;
    }

    @SuppressWarnings({"JpaModelErrorInspection"})
    public boolean isNew() {
        return (id == null);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.kudos.dao;

import in.kudos.domain.Users;
import java.util.List;
import org.springframework.orm.ObjectRetrievalFailureException;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author @rn@b
 */
public interface UsersDao {

    public Users save(Users users);

    public Users loadById(Long id) throws ObjectRetrievalFailureException;

    public List<Users> loadAll();

    @Transactional
    void delete(Users users);

}

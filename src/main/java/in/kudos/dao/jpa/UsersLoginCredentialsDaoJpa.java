/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.kudos.dao.jpa;

import in.kudos.dao.UsersLoginCredentialsDao;
import in.kudos.domain.UsersLoginCredentials;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.springframework.orm.ObjectRetrievalFailureException;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author @rn@b
 */
public class UsersLoginCredentialsDaoJpa implements UsersLoginCredentialsDao {

    @PersistenceContext
    private EntityManager entityManager;

    public EntityManager getEntityManager() {

        return entityManager;
    }

    protected final UsersLoginCredentials newPrototype(Class<UsersLoginCredentials> cl) throws IllegalArgumentException {
        try {
            return cl.newInstance();
        } catch (InstantiationException e) {
            throw new IllegalArgumentException(e);
        } catch (IllegalAccessException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    @Transactional
    public UsersLoginCredentials save(UsersLoginCredentials usersLoginCredentials) {
        if (usersLoginCredentials.getId() != null) {
            getEntityManager().merge(usersLoginCredentials);
        } else {
            getEntityManager().persist(usersLoginCredentials);
        }
        return usersLoginCredentials;
    }

    @Override
    public UsersLoginCredentials loadById(Long id) throws ObjectRetrievalFailureException {
        UsersLoginCredentials usersLoginCredentials = getEntityManager().find(UsersLoginCredentials.class, id);
        if (usersLoginCredentials == null) {
        }
        return usersLoginCredentials;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<UsersLoginCredentials> loadAll() {
        Query query = entityManager.createQuery("select ulc from UsersLoginCredentials as ulc order by ulc.id desc");
        //noinspection unchecked
//        System.out.println("Entity Name");
        return query.getResultList();
    }

    @Override
    public void delete(UsersLoginCredentials usersLoginCredentials) {
        UsersLoginCredentials loadedEntity = loadById(usersLoginCredentials.getId());
        getEntityManager().remove(loadedEntity);
    }

    @Override
    public List<UsersLoginCredentials> checkUsersByUName(String userName) {
        Query query = getEntityManager().createQuery("SELECT ulc FROM UsersLoginCredentials AS ulc WHERE ulc.userName =:userName");
        query.setParameter("userName", userName);
        return query.getResultList();
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.kudos.dao.jpa;

import in.kudos.dao.UsersFriendListDao;
import in.kudos.domain.UsersFriendList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.springframework.orm.ObjectRetrievalFailureException;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author @rn@b
 */
public class UsersFriendListDaoJpa implements UsersFriendListDao {

    @PersistenceContext
    private EntityManager entityManager;

    public EntityManager getEntityManager() {

        return entityManager;
    }

    protected final UsersFriendList newPrototype(Class<UsersFriendList> cl) throws IllegalArgumentException {
        try {
            return cl.newInstance();
        } catch (InstantiationException e) {
            throw new IllegalArgumentException(e);
        } catch (IllegalAccessException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    @Transactional
    public UsersFriendList save(UsersFriendList usersFriendList) {
        if (usersFriendList.getId() != null) {
            getEntityManager().merge(usersFriendList);
        } else {
            getEntityManager().persist(usersFriendList);
        }
        return usersFriendList;
    }

    @Override
    public UsersFriendList loadById(Long id) throws ObjectRetrievalFailureException {
        UsersFriendList usersFriendList = getEntityManager().find(UsersFriendList.class, id);
        if (usersFriendList == null) {
        }
        return usersFriendList;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<UsersFriendList> loadAll() {
        Query query = entityManager.createQuery("select ufl from UsersFriendList as ufl order by ufl.id desc");
        //noinspection unchecked
//        System.out.println("Entity Name");
        return query.getResultList();
    }

    @Override
    public void delete(UsersFriendList usersFriendList) {
        UsersFriendList loadedEntity = loadById(usersFriendList.getId());
        getEntityManager().remove(loadedEntity);
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.kudos.dao.jpa;

import in.kudos.dao.UsersDao;
import in.kudos.domain.Users;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.springframework.orm.ObjectRetrievalFailureException;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author @rn@b
 */
public class UsersDaoJpa implements UsersDao {

    @PersistenceContext
    private EntityManager entityManager;

    public EntityManager getEntityManager() {

        return entityManager;
    }

    protected final Users newPrototype(Class<Users> cl) throws IllegalArgumentException {
        try {
            return cl.newInstance();
        } catch (InstantiationException e) {
            throw new IllegalArgumentException(e);
        } catch (IllegalAccessException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    @Transactional
    public Users save(Users users) {
        if (users.getId() != null) {
            getEntityManager().merge(users);
        } else {
            getEntityManager().persist(users);
        }
        return users;
    }

    @Override
    public Users loadById(Long id) throws ObjectRetrievalFailureException {
        Users users = getEntityManager().find(Users.class, id);
        if (users == null) {
        }
        return users;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Users> loadAll() {
        Query query = entityManager.createQuery("select u from Users as u order by u.id desc");
        //noinspection unchecked
//        System.out.println("Entity Name");
        return query.getResultList();
    }

    @Override
    public void delete(Users users) {
        Users loadedEntity = loadById(users.getId());
        getEntityManager().remove(loadedEntity);
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.kudos.dao.jpa;

import in.kudos.dao.UsersPostListDao;
import in.kudos.domain.UsersPostList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.springframework.orm.ObjectRetrievalFailureException;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author @rn@b
 */
public class UsersPostListDaoJpa implements UsersPostListDao {

    @PersistenceContext
    private EntityManager entityManager;

    public EntityManager getEntityManager() {

        return entityManager;
    }

    protected final UsersPostList newPrototype(Class<UsersPostList> cl) throws IllegalArgumentException {
        try {
            return cl.newInstance();
        } catch (InstantiationException e) {
            throw new IllegalArgumentException(e);
        } catch (IllegalAccessException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    @Transactional
    public UsersPostList save(UsersPostList usersPostList) {
        if (usersPostList.getId() != null) {
            getEntityManager().merge(usersPostList);
        } else {
            getEntityManager().persist(usersPostList);
        }
        return usersPostList;
    }

    @Override
    public UsersPostList loadById(Long id) throws ObjectRetrievalFailureException {
        UsersPostList usersPostList = getEntityManager().find(UsersPostList.class, id);
        if (usersPostList == null) {
        }
        return usersPostList;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<UsersPostList> loadAll() {
        Query query = entityManager.createQuery("select upl from UsersPostList as upl order by upl.id desc");
        //noinspection unchecked
//        System.out.println("Entity Name");
        return query.getResultList();
    }

    @Override
    public void delete(UsersPostList usersPostList) {
        UsersPostList loadedEntity = loadById(usersPostList.getId());
        getEntityManager().remove(loadedEntity);
    }

    @Override
    public List<UsersPostList> loadAllKudosInSorted() {
        Query query = entityManager.createQuery("select upl from UsersPostList as upl order by upl.createUtc desc");
        return query.getResultList();
    }

}

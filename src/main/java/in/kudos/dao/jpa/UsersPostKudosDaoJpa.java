/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.kudos.dao.jpa;

import in.kudos.dao.UsersPostKudosDao;
import in.kudos.domain.Users;
import in.kudos.domain.UsersPostKudos;
import in.kudos.domain.UsersPostList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.springframework.orm.ObjectRetrievalFailureException;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author @rn@b
 */
public class UsersPostKudosDaoJpa implements UsersPostKudosDao {

    @PersistenceContext
    private EntityManager entityManager;

    public EntityManager getEntityManager() {

        return entityManager;
    }

    protected final UsersPostKudos newPrototype(Class<UsersPostKudos> cl) throws IllegalArgumentException {
        try {
            return cl.newInstance();
        } catch (InstantiationException e) {
            throw new IllegalArgumentException(e);
        } catch (IllegalAccessException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    @Transactional
    public UsersPostKudos save(UsersPostKudos usersPostKudos) {
        if (usersPostKudos.getId() != null) {
            getEntityManager().merge(usersPostKudos);
        } else {
            getEntityManager().persist(usersPostKudos);
        }
        return usersPostKudos;
    }

    @Override
    public UsersPostKudos loadById(Long id) throws ObjectRetrievalFailureException {
        UsersPostKudos usersPostKudos = getEntityManager().find(UsersPostKudos.class, id);
        if (usersPostKudos == null) {
        }
        return usersPostKudos;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<UsersPostKudos> loadAll() {
        Query query = entityManager.createQuery("select upk from UsersPostKudos as upk order by upk.id desc");
        //noinspection unchecked
//        System.out.println("Entity Name");
        return query.getResultList();
    }

    @Override
    public void delete(UsersPostKudos usersPostKudos) {
        UsersPostKudos loadedEntity = loadById(usersPostKudos.getId());
        getEntityManager().remove(loadedEntity);
    }

    @Override
    public List<UsersPostKudos> getUserLikedPostByUserId(Users users, UsersPostList list) {
        Query query = getEntityManager().createQuery("select upk from UsersPostKudos as upk WHERE upk.userId =:users AND upk.userPostId =:list");
        query.setParameter("users", users);
        query.setParameter("list", list);
        return query.getResultList();
    }

    @Override
    public List<UsersPostKudos> getTotalLikesForAKudos(UsersPostList list) {
        Query query = getEntityManager().createQuery("select upk from UsersPostKudos as upk WHERE upk.userPostId =:list ORDER BY upk.createUtc DESC");
        query.setParameter("list", list);
        return query.getResultList();
    }

}

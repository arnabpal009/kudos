/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.kudos.dao;

import in.kudos.domain.UsersLoginCredentials;
import java.util.List;
import org.springframework.orm.ObjectRetrievalFailureException;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author @rn@b
 */
public interface UsersLoginCredentialsDao {

    public UsersLoginCredentials save(UsersLoginCredentials usersLoginCredentials);

    public UsersLoginCredentials loadById(Long id) throws ObjectRetrievalFailureException;

    public List<UsersLoginCredentials> loadAll();

    @Transactional
    void delete(UsersLoginCredentials usersLoginCredentials);

    public List<UsersLoginCredentials> checkUsersByUName(String userName);

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.kudos.dao;

import in.kudos.domain.UsersPostList;
import java.util.List;
import org.springframework.orm.ObjectRetrievalFailureException;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author @rn@b
 */
public interface UsersPostListDao {

    public UsersPostList save(UsersPostList usersPostList);

    public UsersPostList loadById(Long id) throws ObjectRetrievalFailureException;

    public List<UsersPostList> loadAll();

    @Transactional
    void delete(UsersPostList usersPostList);

    public List<UsersPostList> loadAllKudosInSorted();

}

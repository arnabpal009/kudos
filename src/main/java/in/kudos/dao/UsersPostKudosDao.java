/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.kudos.dao;

import in.kudos.domain.Users;
import in.kudos.domain.UsersPostKudos;
import in.kudos.domain.UsersPostList;
import java.util.List;
import org.springframework.orm.ObjectRetrievalFailureException;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author @rn@b
 */
public interface UsersPostKudosDao {

    public UsersPostKudos save(UsersPostKudos usersPostKudos);

    public UsersPostKudos loadById(Long id) throws ObjectRetrievalFailureException;

    public List<UsersPostKudos> loadAll();

    @Transactional
    void delete(UsersPostKudos usersPostKudos);

    public List<UsersPostKudos> getUserLikedPostByUserId(Users users, UsersPostList list);

    public List<UsersPostKudos> getTotalLikesForAKudos(UsersPostList list);

}
